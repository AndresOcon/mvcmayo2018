<?php  
//Fichero models/blogModel.php

class Blog{

	public $entradas;

	public function __construct(){
		$this->entradas=[];
	}

	public function dimeEntradas(){
		global $conexion;//Hago alusión a la conexion global
		$sql="SELECT * FROM blog ORDER BY fecha DESC";
		$consulta=$conexion->query($sql);
		while ($registro=$consulta->fetch_array()) {
			$this->entradas[]=new Post($registro);
		}
		return $this->entradas; //Devuelvo un array de POSTS

	}

	public function dimeEntrada($id){
		global $conexion;//Hago alusión a la conexion global
		$sql="SELECT * FROM blog WHERE id=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array(); 
		$entrada=new Post($registro);
		return $entrada;//Devuelvo un solo POST

	}

} //Fin de la class Blog
?>