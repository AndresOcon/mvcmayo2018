<?php  
//Fichero models/postModel.php

class Post{

	public $id;
	public $titulo;
	public $contenido;
	public $autor;
	public $fecha;

	public function __construct($registro){
		$this->id=$registro['id'];
		$this->titulo=$registro['titulo'];
		$this->contenido=$registro['contenido'];
		$this->autor=$registro['autor'];
		$this->fecha=$registro['fecha'];
	}

} //Fin de la class Post
?>