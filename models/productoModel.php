<?php  
//Fichero models/productoModel.php

class Producto{

	public $idProd;
	public $nombreProd;
	public $descripcionProd;
	public $precioProd;
	public $unidadesProd;
	public $fecha;

	public function __construct($registro){
		$this->idProd=$registro['idProd'];
		$this->nombreProd=$registro['nombreProd'];
		$this->descripcionProd=$registro['descripcionProd'];
		$this->precioProd=$registro['precioProd'];
		$this->unidadesProd=$registro['unidadesProd'];
		$this->fecha=$registro['fechaAlta'];
	}

} //Fin de la class Producto
?>