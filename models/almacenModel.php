<?php  
//Fichero models/almacenModel.php

class Almacen{

	public $elementos;

	public function __construct(){
		$this->elementos=[];
	}

	public function dimeProductos(){
		global $conexion;//Hago alusión a la conexion global
		$sql="SELECT * FROM productos ORDER BY fechaAlta DESC";
		$consulta=$conexion->query($sql);
		while ($registro=$consulta->fetch_array()) {
			$this->elementos[]=new Producto($registro);
		}
		return $this->elementos; //Devuelvo un array de productos

	}

	public function dimeProducto($idProd){
		global $conexion;//Hago alusión a la conexion global
		$sql="SELECT * FROM productos WHERE idProd=$idProd";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array(); 
		$elemento=new Producto($registro);
		return $elemento;//Devuelvo un solo PRODUCTO

	}

} //Fin de la class Almacen
?>