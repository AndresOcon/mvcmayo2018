<?php  
//Fichero controllers/blogController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/postModel.php');
require('models/blogModel.php');
$blog=new Blog();

//Como ya he traido  mi modelo de datos, extraigo los datos
// que luego le pasare a la vista
if(isset($_GET['id'])){
	$entrada=$blog->dimeEntrada($_GET['id']);
	echo $twig->render('entrada.html.twig', Array('entrada'=>$entrada));	
}else{
	$entradas=$blog->dimeEntradas();
	echo $twig->render('entradas.html.twig', Array('entradas'=>$entradas));
}

?>