<?php  
//Fichero controllers/productosController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/productoModel.php');
require('models/almacenModel.php');
$almacen=new Almacen();

//Como ya he traido  mi modelo de datos, extraigo los datos
// que luego le pasare a la vista
if(isset($_GET['id'])){
	$producto=$almacen->dimeProducto($_GET['id']);
	echo $twig->render('producto.html.twig', Array('producto'=>$producto));	
}else{
	$productos=$almacen->dimeProductos();
	echo $twig->render('productos.html.twig', Array('productos'=>$productos));
}

?>