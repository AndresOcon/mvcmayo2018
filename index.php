<?php
//Conectamos con la base de datos
require('includes/conexion.php');

//LLamamos a las librerías que hay en /vendor/
require('vendor/autoload.php');

//Le decimos donde irán las plantillas de twig
$loader=new Twig_Loader_Filesystem('views/');

//Desarrollo
$twig = new Twig_Environment($loader);

//Producción
//$twig = new Twig_Environment($loader, array('cache' => 'cache/'));

//LLamamos a un controlador
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='productosController.php';
}

require('controllers/'.$c);

//Desconectar
$conexion->close();


?>